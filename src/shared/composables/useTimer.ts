import {onBeforeUnmount, ref} from "vue";

export const useTimer = (s:number) => {
    const timer = ref(0);
    let intervalId: number;
    const stopTimer = () => {
        clearInterval(intervalId);
    }
    const startTimer = () => {
        timer.value = s;
        if (intervalId) clearInterval(intervalId);
        intervalId = setInterval(() => {
            timer.value--;
            if (timer.value === 0) {
                clearInterval(intervalId);
            }
        }, 1000);
    };
    onBeforeUnmount(()=>{
        stopTimer();
    })
    return {
        timer,
        startTimer,
        stopTimer
    };
};