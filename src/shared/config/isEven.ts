export const isEvenFromWord = (word: string): boolean => {
  return word.replace(/\s/g, '').length % 2 === 0;
}