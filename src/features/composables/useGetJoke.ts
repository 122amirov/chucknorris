import {randomJokeService} from "../api/randomJokeService.ts";
import {ref} from "vue";
import {IJoke} from "../model/types.ts";

export const useGetJoke = () => {
    const jokeAnswer = ref<IJoke>({
        value: "",
        id: "",
        url: "",
        icon_url: "",
    });
    const loading = ref(false);
    const isError = ref(false);
    const getJoke = async () => {
        try {
            loading.value = true;
            const response = await randomJokeService.getJoke();
            jokeAnswer.value = response.data
        }catch (e) {
            console.log(e);
            isError.value = true;
            throw e;
        }finally {
            loading.value = false;
        }
    };

    return {
        jokeAnswer,
        getJoke,
        loading,
        isError,
    }
}