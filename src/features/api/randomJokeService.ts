import http from "../../app/config/axios.ts";
import {IJoke} from "../model/types.ts";

class RandomJokeService {
    getJoke() {
        return http.get<IJoke>("/jokes/random");
    }
}

export const randomJokeService = new RandomJokeService();